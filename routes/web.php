<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// website route
Route::get('/', function () {
    return view('website.index');
});


Route::get('/contact', function () {
    return view('website.contact');
});



Route::get('/login', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// Auth::routes();

// Route::get('/dashboard', 'HomeController@index')->name('dashboard.html');

// Route::get('layout-api', [
//      'as' => 'layout-api',
// ]);

// // layoutSidebar Route
Route::get('/layout-api', function () {
    return view('dashboard/layoutSidebar/layout-api');
});

Route::get('/layout-collapsed-sidebar', function () {
    return view('dashboard/layoutSidebar/layout-collapsed-sidebar');
});


Route::get('/layout-fixed-sidebar', function () {
    return view('dashboard/layoutSidebar/layout-fixed-sidebar');
});

Route::get('/layout-chat-open', function () {
    return view('dashboard/layoutSidebar/layout-chat-open');
});

Route::get('/layout-horizontal-menu-boxed', function () {
    return view('dashboard/layoutSidebar/layout-horizontal-menu-boxed');
});


Route::get('/layout-horizontal-menu-fluid', function () {
    return view('dashboard/layoutSidebar/layout-horizontal-menu-fluid');
});

Route::get('/layout-mixed-menus', function () {
    return view('dashboard/layoutSidebar/layout-mixed-menus');
});

Route::get('/layout-right-sidebar', function () {
    return view('dashboard/layoutSidebar/layout-right-sidebar');
});

Route::get('/layout-both-menus-right-sidebar', function () {
    return view('dashboard/layoutSidebar/layout-both-menus-right-sidebar');
});

Route::get('/layout-page-transition-fade', function () {
    return view('dashboard/layoutSidebar/layout-page-transition-fade');
});

Route::get('/layout-page-transition-left-in', function () {
    return view('dashboard/layoutSidebar/layout-page-transition-left-in');
});

Route::get('/layout-page-transition-right-in', function () {
    return view('dashboard/layoutSidebar/layout-page-transition-right-in');
});

Route::get('/layout-page-transition-fade-only', function () {
    return view('dashboard/layoutSidebar/layout-page-transition-fade-only');
});

Route::get('/layout-boxed', function () {
    return view('dashboard/layoutSidebar/layout-boxed');
});

// // skins Route
Route::get('/skin-black', function () {
    return view('dashboard/skins/skin-black');
});
Route::get('/skin-blue', function () {
    return view('dashboard/skins/skin-blue');
});
Route::get('/skin-cafe', function () {
    return view('dashboard/skins/skin-cafe');
});
Route::get('/skin-facebook', function () {
    return view('dashboard/skins/skin-facebook');
});
Route::get('/skin-green', function () {
    return view('dashboard/skins/skin-green');
});
Route::get('/skin-purple', function () {
    return view('dashboard/skins/skin-purple');
});
Route::get('/skin-red', function () {
    return view('dashboard/skins/skin-red');
});
Route::get('/skin-white', function () {
    return view('dashboard/skins/skin-white');
});
Route::get('/skin-yellow', function () {
    return view('dashboard/skins/skin-yellow');
});

// // Mailbox Route
Route::get('/mailbox-compose', function () {
    return view('dashboard/Mailbox/mailbox-compose');
});
Route::get('/mailbox-message', function () {
    return view('dashboard/Mailbox/mailbox-message');
});

Route::get('/mailbox', function () {
    return view('dashboard/Mailbox/mailbox');
});
// // Forms Route
Route::get('/forms-advanced', function () {
    return view('dashboard/Forms/forms-advanced');
});
Route::get('/forms-buttons', function () {
    return view('dashboard/Forms/forms-buttons');
});
Route::get('/forms-file-upload', function () {
    return view('dashboard/Forms/forms-file-upload');
});
Route::get('/forms-icheck', function () {
    return view('dashboard/Forms/forms-icheck');
});
Route::get('/forms-main', function () {
    return view('dashboard/Forms/forms-main');
});
Route::get('/forms-masks', function () {
    return view('dashboard/Forms/forms-masks');
});
Route::get('/forms-sliders', function () {
    return view('dashboard/Forms/forms-sliders');
});
Route::get('/forms-validation', function () {
    return view('dashboard/Forms/forms-validation');
});
Route::get('/forms-wizard', function () {
    return view('dashboard/Forms/forms-wizard');
});

// // tables Route
Route::get('/tables-datatable', function () {
    return view('dashboard/Tables/tables-datatable');
});
Route::get('/tables-main', function () {
    return view('dashboard/Tables/tables-main');
});

// // Extra Route
Route::get('/extra-404', function () {
    return view('dashboard/Extra/extra-404');
});
Route::get('/extra-blank', function () {
    return view('dashboard/Extra/extra-blank');
});
Route::get('/extra-calendar', function () {
    return view('dashboard/Extra/extra-calendar');
});
Route::get('/extra-comments', function () {
    return view('dashboard/Extra/extra-comments');
});
Route::get('/extra-forgot-password', function () {
    return view('dashboard/Extra/extra-forgot-password');
});
Route::get('/extra-gallery-single', function () {
    return view('dashboard/Extra/extra-gallery-single');
});
Route::get('/extra-gallery', function () {
    return view('dashboard/Extra/extra-gallery');
});
Route::get('/extra-google-maps', function () {
    return view('dashboard/Extra/extra-google-maps');
});
Route::get('/extra-icons', function () {
    return view('dashboard/Extra/extra-icons');
});
Route::get('/extra-image-crop', function () {
    return view('dashboard/Extra/extra-image-crop');
});
Route::get('/extra-invoice', function () {
    return view('dashboard/Extra/extra-invoice');
});
Route::get('/extra-lockscreen', function () {
    return view('dashboard/Extra/extra-lockscreen');
});
Route::get('/extra-members', function () {
    return view('dashboard/Extra/extra-members');
});
Route::get('/extra-notes', function () {
    return view('dashboard/Extra/extra-notes');
});
Route::get('/extra-profile', function () {
    return view('dashboard/Extra/extra-profile');
});
Route::get('/extra-register', function () {
    return view('dashboard/Extra/extra-register');
});
Route::get('/extra-scrollbox', function () {
    return view('dashboard/Extra/extra-scrollbox');
});
Route::get('/extra-search', function () {
    return view('dashboard/Extra/extra-search');
});
Route::get('/extra-settings', function () {
    return view('dashboard/Extra/extra-settings');
});
Route::get('/extra-timeline', function () {
    return view('dashboard/Extra/extra-timeline');
});

// // UI Elements
Route::get('/ui-alerts', function () {
    return view('dashboard/UI Elements/ui-alerts');
});
Route::get('/ui-badges-labels', function () {
    return view('dashboard/UI Elements/ui-alerts');
});
Route::get('/ui-blockquotes', function () {
    return view('dashboard/UI Elements/ui-blockquotes');
});
Route::get('/ui-modals', function () {
    return view('dashboard/UI Elements/ui-modals');
});
Route::get('/ui-navbars', function () {
    return view('dashboard/UI Elements/ui-navbars');
});
Route::get('/ui-notifications', function () {
    return view('dashboard/UI Elements/ui-notifications');
});
Route::get('/ui-panels', function () {
    return view('dashboard/UI Elements/ui-panels');
});
Route::get('/ui-progress-bars', function () {
    return view('dashboard/UI Elements/ui-progress-bars');
});
Route::get('/ui-tiles', function () {
    return view('dashboard/UI Elements/ui-tiles');
});
Route::get('/ui-tooltips-popovers', function () {
    return view('dashboard/UI Elements/ui-tooltips-popovers');
});

// Add restaurant
// Route::resource('restaurant', 'RestaurantController');

// Route::get('/restaurant', function () {
//     return view('dashboard/restaurant/add_restaurant');
// });

Route::get('/restaurant/create','RestaurantController@create');

Route::get('/restaurant/ajax/{id}',array('as'=>'restaurant.ajax','uses'=>'RestaurantController@myformAjax'));



