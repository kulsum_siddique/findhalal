<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="#">
						<img src="{{asset('assets/images/logo@2x.png')}}" width="120" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="active opened active has-sub">
					<a href="index.html">
						<i class="entypo-gauge"></i>
						<span class="title">Dashboard</span>
					</a>
					<ul class="visible">
						<li class="has-sub">
							<a href="/skin-black">
								<span class="title">Skins</span>
							</a>
							<ul>
								<li>
									<a href="/skin-black">
										<span class="title">Black Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-white">
										<span class="title">White Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-purple">
										<span class="title">Purple Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-cafe">
										<span class="title">Cafe Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-red">
										<span class="title">Red Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-green">
										<span class="title">Green Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-yellow.">
										<span class="title">Yellow Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-blue">
										<span class="title">Blue Skin</span>
									</a>
								</li>
								<li>
									<a href="/skin-facebook">
										<span class="title">Facebook Skin</span>
										<span class="badge badge-secondary badge-roundless">New</span>
									</a>
								</li>
							</ul>
						</li>
				
					</ul>
				</li>
				<li class="has-sub">
					<a href="/layout-api">
						<i class="entypo-layout"></i>
						<span class="title">Layouts</span>
					</a>
					<ul>
						<li>
							<a href="/layout-api">
								<span class="title">Layout API</span>
							</a>
						</li>
				
						<li>
							<a href="/layout-fixed-sidebar">
								<span class="title">Fixed Sidebar</span>
							</a>
						</li>
						<li>
							<a href="/layout-chat-open">
								<span class="title">Chat Open</span>
							</a>
						</li>
						<li>
							<a href="/layout-horizontal-menu-boxed">
								<span class="title">Horizontal Menu Boxed</span>
							</a>
						</li>
						<li>
							<a href="/layout-horizontal-menu-fluid">
								<span class="title">Horizontal Menu Fluid</span>
							</a>
						</li>
						
						<li>
							<a href="/layout-right-sidebar">
								<span class="title">Right Sidebar</span>
							</a>
						</li>
						<li>
							<a href="/layout-both-menus-right-sidebar">
								<span class="title">Both Menus (Right Sidebar)</span>
							</a>
						</li>
						<li class="has-sub">
							<a href="layout-page-transition-fade.html">
								<span class="title">Page Enter Transitions</span>
							</a>
							<ul>
								<li>
									<a href="/layout-page-transition-fade">
										<span class="title">Fade Scale</span>
									</a>
								</li>
								<li>
									<a href="/layout-page-transition-left-in">
										<span class="title">Left In</span>
									</a>
								</li>
								<li>
									<a href="/layout-page-transition-right-in">
										<span class="title">Right In</span>
									</a>
								</li>
								<li>
									<a href="/layout-page-transition-fade-only">
										<span class="title">Fade Only</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="/layout-boxed">
								<span class="title">Boxed Layout</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#" target="_blank">
						<i class="entypo-monitor"></i>
						<span class="title">Frontend</span>
					</a>
				</li>
				<li class="has-sub">
					<a href="/ui-panels">
						<i class="entypo-newspaper"></i>
						<span class="title">UI Elements</span>
					</a>
					<ul>
						<li>
							<a href="/ui-panels">
								<span class="title">Panels</span>
							</a>
						</li>
						
						<li>
							<a href="/ui-tooltips-popovers">
								<span class="title">Tooltips &amp; Popovers</span>
							</a>
						</li>
						<li>
							<a href="/ui-navbars">
								<span class="title">Navbars</span>
							</a>
						</li>
						
						<li>
							<a href="/ui-badges-labels">
								<span class="title">Badges &amp; Labels</span>
							</a>
						</li>
						<li>
							<a href="/ui-progress-bars">
								<span class="title">Progress Bars</span>
							</a>
						</li>
						<li>
							<a href="/ui-modals">
								<span class="title">Modals</span>
							</a>
						</li>
						<li>
							<a href="/ui-blockquotes">
								<span class="title">Blockquotes</span>
							</a>
						</li>
						<li>
							<a href="/ui-alerts">
								<span class="title">Alerts</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has-sub">
					<a href="/mailbox">
						<i class="entypo-mail"></i>
						<span class="title">Mailbox</span>
						<span class="badge badge-secondary">8</span>
					</a>
					<ul>
						<li>
							<a href="/mailbox">
								<i class="entypo-inbox"></i>
								<span class="title">Inbox</span>
							</a>
						</li>
						<li>
							<a href="/mailbox-compose">
								<i class="entypo-pencil"></i>
								<span class="title">Compose Message</span>
							</a>
						</li>
						<li>
							<a href="/mailbox-message">
								<i class="entypo-attach"></i>
								<span class="title">View Message</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="/forms-main">
						<i class="entypo-doc-text"></i>
						<span class="title">Forms</span>
					</a>
					<ul>
						<li>
							<a href="/forms-main">
								<span class="title">Basic Elements</span>
							</a>
						</li>
						<li>
							<a href="/forms-advanced">
								<span class="title">Advanced Plugins</span>
							</a>
						</li>
						<li>
							<a href="/forms-wizard">
								<span class="title">Form Wizard</span>
							</a>
						</li>
						<li>
							<a href="/forms-validation">
								<span class="title">Data Validation</span>
							</a>
						</li>
						<li>
							<a href="/forms-masks">
								<span class="title">Input Masks</span>
							</a>
						</li>
						<li>
							<a href="/forms-sliders">
								<span class="title">Sliders</span>
							</a>
						</li>
						<li>
							<a href="/forms-file-upload">
								<span class="title">File Upload</span>
							</a>
						</li>
						<li>
							<a href="/forms-wysiwyg">
								<span class="title">Editors</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="/tables-main">
						<i class="entypo-window"></i>
						<span class="title">Tables</span>
					</a>
					<ul>
						<li>
							<a href="/tables-main">
								<span class="title">Basic Tables</span>
							</a>
						</li>
						<li>
							<a href="/tables-datatable">
								<span class="title">Data Tables</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="/extra-icons">
						<i class="entypo-bag"></i>
						<span class="title">Extra</span>
						<span class="badge badge-info badge-roundless">New Items</span>
					</a>
					<ul>
						<li class="has-sub">
							<a href="/extra-icons">
								<span class="title">Icons</span>
								<span class="badge badge-success">3</span>
							</a>
							<ul>
								<li>
									<a href="/extra-icons">
										<span class="title">Font Awesome</span>
									</a>
								</li>
								<li>
									<a href="/extra-icons-entypo">
										<span class="title">Entypo</span>
									</a>
								</li>
								<li>
									<a href="/extra-icons-glyphicons">
										<span class="title">Glyph Icons</span>
									</a>
								</li>
							</ul>
						</li>
						
						
						
						<li>
							<a href="/extra-calendar">
								<span class="title">Calendar</span>
							</a>
						</li>
						
						<li>
							<a href="/extra-notes">
								<span class="title">Notes</span>
							</a>
						</li>
						<li>
							<a href="/extra-lockscreen">
								<span class="title">Lockscreen</span>
							</a>
						</li>
						<li>
							<a href="/extra-login">
								<span class="title">Login</span>
							</a>
						</li>
						<li>
							<a href="/extra-register">
								<span class="title">Register</span>
							</a>
						</li>
						<li>
							<a href="/extra-invoice">
								<span class="title">Invoice</span>
							</a>
						</li>
						<li class="has-sub">
							<a href="/extra-gallery">
								<span class="title">Gallery</span>
							</a>
							<ul>
								<li>
									<a href="/extra-gallery">
										<span class="title">Albums</span>
									</a>
								</li>
								<li>
									<a href="/extra-gallery-single">
										<span class="title">Single Album</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="/extra-members">
								<span class="title">Members</span>
							</a>
						</li>
						<li>
							<a href="/extra-profile">
								<span class="title">Profile</span>
							</a>
						</li>
						
						
						<li>
							<a href="/extra-comments">
								<span class="title">Comments</span>
							</a>
						</li>
						
						
						
						<li>
							<a href="/extra-scrollbox">
								<span class="title">Scrollbox</span>
							</a>
						</li>
						
						<li>
							<a href="/extra-search">
								<span class="title">Search Page</span>
							</a>
						</li>
						
						
					</ul>
				</li>
				<li>
					<a href="/charts">
						<i class="entypo-chart-bar"></i>
						<span class="title">Charts</span>
					</a>
				</li>
				<li class="has-sub">
					<a href="#">
						<i class="entypo-flow-tree"></i>
						<span class="title">Menu Levels</span>
					</a>
					<ul>
						<li>
							<a href="#">
								<i class="entypo-flow-line"></i>
								<span class="title">Menu Level 1.1</span>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="entypo-flow-line"></i>
								<span class="title">Menu Level 1.2</span>
							</a>
						</li>
						<li class="has-sub">
							<a href="#">
								<i class="entypo-flow-line"></i>
								<span class="title">Menu Level 1.3</span>
							</a>
							<ul>
								<li>
									<a href="#">
										<i class="entypo-flow-parallel"></i>
										<span class="title">Menu Level 2.1</span>
									</a>
								</li>
								<li class="has-sub">
									<a href="#">
										<i class="entypo-flow-parallel"></i>
										<span class="title">Menu Level 2.2</span>
									</a>
									<ul>
										<li class="has-sub">
											<a href="#">
												<i class="entypo-flow-cascade"></i>
												<span class="title">Menu Level 3.1</span>
											</a>
											<ul>
												<li>
													<a href="#">
														<i class="entypo-flow-branch"></i>
														<span class="title">Menu Level 4.1</span>
													</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="#">
												<i class="entypo-flow-cascade"></i>
												<span class="title">Menu Level 3.2</span>
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">
										<i class="entypo-flow-parallel"></i>
										<span class="title">Menu Level 2.3</span>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="has-sub">
					<a href="/restaurant/create">
					
						<span class="title">Restaurant</span>
						
					</a>
					<ul>
						<li>
							<a href="/restaurant/create">
								
								<span class="title">Restaurant</span>
							</a>
						</li>
						<!-- <li>
							<a href="/mailbox-compose">
								<i class="entypo-pencil"></i>
								<span class="title">Compose Message</span>
							</a>
						</li> -->
						<!-- <li>
							<a href="/mailbox-message">
								<i class="entypo-attach"></i>
								<span class="title">View Message</span>
							</a>
						</li> -->
					</ul>
				</li>
			</ul>
			
		</div>

	</div>