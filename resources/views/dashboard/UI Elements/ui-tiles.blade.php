<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Findhalal | Tiles</title>

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	

@include('dashboard.sidebar')

<div class="main-content">

	<div class="row">

		<!-- Profile Info and Notifications -->
		<div class="col-md-6 col-sm-8 clearfix">

			<ul class="user-info pull-left pull-none-xsm">

				<!-- Profile Info -->
				<li class="profile-info dropdown">
					<!-- add class "pull-right" if you want to place this from right -->

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<h2>Findhalal </h2>
					</a>

					<ul class="dropdown-menu">

						<!-- Reverse Caret -->
						<li class="caret"></li>

						<!-- Profile sub-links -->
						<li>
							<a href="extra-timeline.html">
							<i class="entypo-user"></i>
							Edit Profile
						</a>
						</li>

						<li>
							<a href="mailbox.html">
							<i class="entypo-mail"></i>
							Inbox
						</a>
						</li>

						<li>
							<a href="extra-calendar.html">
							<i class="entypo-calendar"></i>
							Calendar
						</a>
						</li>

						<li>
							<a href="#">
							<i class="entypo-clipboard"></i>
							Tasks
						</a>
						</li>
					</ul>
				</li>

			</ul>

			<ul class="user-info pull-left pull-right-xs pull-none-xsm">

				<!-- Raw Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-attention"></i>
					<span class="badge badge-info">6</span>
				</a>

					<ul class="dropdown-menu">
						<li class="top">
							<p class="small">
								<a href="#" class="pull-right">Mark all Read</a> You have <strong>3</strong> new notifications.
							</p>
						</li>

						<li>
							<ul class="dropdown-menu-list scroller">
								<li class="unread notification-success">
									<a href="#">
									<i class="entypo-user-add pull-right"></i>
									
									<span class="line">
										<strong>New user registered</strong>
									</span>
									
									<span class="line small">
										30 seconds ago
									</span>
								</a>
								</li>

								<li class="unread notification-secondary">
									<a href="#">
									<i class="entypo-heart pull-right"></i>
									
									<span class="line">
										<strong>Someone special liked this</strong>
									</span>
									
									<span class="line small">
										2 minutes ago
									</span>
								</a>
								</li>

								<li class="notification-primary">
									<a href="#">
									<i class="entypo-user pull-right"></i>
									
									<span class="line">
										<strong>Privacy settings have been changed</strong>
									</span>
									
									<span class="line small">
										3 hours ago
									</span>
								</a>
								</li>

								<li class="notification-danger">
									<a href="#">
									<i class="entypo-cancel-circled pull-right"></i>
									
									<span class="line">
										John cancelled the event
									</span>
									
									<span class="line small">
										9 hours ago
									</span>
								</a>
								</li>

								<li class="notification-info">
									<a href="#">
									<i class="entypo-info pull-right"></i>
									
									<span class="line">
										The server is status is stable
									</span>
									
									<span class="line small">
										yesterday at 10:30am
									</span>
								</a>
								</li>

								<li class="notification-warning">
									<a href="#">
									<i class="entypo-rss pull-right"></i>
									
									<span class="line">
										New comments waiting approval
									</span>
									
									<span class="line small">
										last week
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="#">View all notifications</a>
						</li>
					</ul>

				</li>

				<!-- Message Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-mail"></i>
					<span class="badge badge-secondary">10</span>
				</a>

					<ul class="dropdown-menu">
						<li>
							<form class="top-dropdown-search">

								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search anything..." name="s" />
								</div>

							</form>

							<ul class="dropdown-menu-list scroller">
								<li class="active">
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-1@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										<strong>Luc Chartier</strong>
										- yesterday
									</span>
									
									<span class="line desc small">
										This ain’t our first item, it is the best of the rest.
									</span>
								</a>
								</li>

								<li class="active">
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-2@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										<strong>Salma Nyberg</strong>
										- 2 days ago
									</span>
									
									<span class="line desc small">
										Oh he decisively impression attachment friendship so if everything. 
									</span>
								</a>
								</li>

								<li>
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-3@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										Hayden Cartwright
										- a week ago
									</span>
									
									<span class="line desc small">
										Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
									</span>
								</a>
								</li>

								<li>
									<a href="#">
									<span class="image pull-right">
										<img src="assets/images/thumb-4@2x.png" width="44" alt="" class="img-circle" />
									</span>
									
									<span class="line">
										Sandra Eberhardt
										- 16 days ago
									</span>
									
									<span class="line desc small">
										On so attention necessary at by provision otherwise existence direction.
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="mailbox.html">All Messages</a>
						</li>
					</ul>

				</li>

				<!-- Task Notifications -->
				<li class="notifications dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-list"></i>
					<span class="badge badge-warning">1</span>
				</a>

					<ul class="dropdown-menu">
						<li class="top">
							<p>You have 6 pending tasks</p>
						</li>

						<li>
							<ul class="dropdown-menu-list scroller">
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Procurement</span>
										<span class="percent">27%</span>
									</span>
								
									<span class="progress">
										<span style="width: 27%;" class="progress-bar progress-bar-success">
											<span class="sr-only">27% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">App Development</span>
										<span class="percent">83%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 83%;" class="progress-bar progress-bar-danger">
											<span class="sr-only">83% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">HTML Slicing</span>
										<span class="percent">91%</span>
									</span>
									
									<span class="progress">
										<span style="width: 91%;" class="progress-bar progress-bar-success">
											<span class="sr-only">91% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Database Repair</span>
										<span class="percent">12%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 12%;" class="progress-bar progress-bar-warning">
											<span class="sr-only">12% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Backup Create Progress</span>
										<span class="percent">54%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 54%;" class="progress-bar progress-bar-info">
											<span class="sr-only">54% Complete</span>
										</span>
									</span>
								</a>
								</li>
								<li>
									<a href="#">
									<span class="task">
										<span class="desc">Upgrade Progress</span>
										<span class="percent">17%</span>
									</span>
									
									<span class="progress progress-striped">
										<span style="width: 17%;" class="progress-bar progress-bar-important">
											<span class="sr-only">17% Complete</span>
										</span>
									</span>
								</a>
								</li>
							</ul>
						</li>

						<li class="external">
							<a href="#">See all tasks</a>
						</li>
					</ul>

				</li>

			</ul>

		</div>


		<!-- Raw Links -->
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">
	
	<ul class="list-inline links-list pull-right">



		<li class="sep"></li>

		<li>
			<a href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				Log Out <i class="entypo-logout right"></i>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			 </form>
		</li>
	</ul>

</div>

</div>

<hr />
		
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="ui-panels.html">UI Elements</a>
							</li>
						<li class="active">
		
									<strong>Tiles</strong>
							</li>
							</ol>
					
		<h2>Stats - Tiles</h2>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-primary">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" &pound;" data-duration="1500" data-delay="0">0 &pound;</div>
					
					<h3>Prefix and Postfix</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-red">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-blue">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-cyan">
					<div class="icon"><i class="entypo-paper-plane"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-purple">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-pink">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-orange">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-green">
					<div class="icon"><i class="entypo-paper-plane"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-brown">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-plum">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-gray">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		
		<br />
		
		<h2>Progress Bar - Tiles</h2>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-progress tile-primary">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="65.5%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-red">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="23.2%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-blue">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="78%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-aqua">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="22%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
		</div>
		
		<div class="row">
		
			<div class="col-sm-3">
			
				<div class="tile-progress tile-green">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="94%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-cyan">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="45.9%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-purple">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="27%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-pink">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="3"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
		</div>
		
		<div class="row">
		
			<div class="col-sm-3">
			
				<div class="tile-progress tile-orange">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="41%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-brown">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="88%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-plum">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="74%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-progress tile-gray">
					
					<div class="tile-header">
						<h3>Visitors</h3>
						<span>so far in our blog, and our website.</span>
					</div>
					
					<div class="tile-progressbar">
						<span data-fill="13.5%"></span>
					</div>
					
					<div class="tile-footer">
						<h4>
							<span class="pct-counter">0</span>% increase
						</h4>
						
						<span>so far in our blog and our website</span>
					</div>
				</div>
			
			</div>
			
		</div>
		
		
		<br />
		
		<h2>Block - Tiles</h2>
		
		<br />
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var $todo_tasks = $("#todo_tasks");
				
				$todo_tasks.find('input[type="text"]').on('keydown', function(ev)
				{
					if(ev.keyCode == 13)
					{
						ev.preventDefault();
						
						if($.trim($(this).val()).length)
						{
							var $todo_entry = $('<li><div class="checkbox checkbox-replace color-white"><input type="checkbox" /><label>'+$(this).val()+'</label></div></li>');
							$(this).val('');
							
							$todo_entry.appendTo($todo_tasks.find('.todo-list'));
							$todo_entry.hide().slideDown('fast');
							replaceCheckboxes();
						}
					}
				});
			});
		</script>
		
		<div class="row">
			
			<div class="col-sm-3">
			
				<div class="tile-block tile-green" id="todo_tasks">
					
					<div class="tile-header">
						<i class="entypo-list"></i>
						
						<a href="#">
							Tasks
							<span>To do list, tick one.</span>
						</a>
					</div>
					
					<div class="tile-content">
						
						<input type="text" class="form-control" placeholder="Add Task" />
						
						
						<ul class="todo-list">
							<li>
								<div class="checkbox checkbox-replace color-white">
									<input type="checkbox" />
									<label>Website Design</label>
								</div>
							</li>
							
							<li>
								<div class="checkbox checkbox-replace color-white">
									<input type="checkbox" id="task-2" checked />
									<label>Slicing</label>
								</div>
							</li>
							
							<li>
								<div class="checkbox checkbox-replace color-white">
									<input type="checkbox" id="task-3" />
									<label>WordPress Integration</label>
								</div>
							</li>
							
							<li>
								<div class="checkbox checkbox-replace color-white">
									<input type="checkbox" id="task-4" />
									<label>SEO Optimize</label>
								</div>
							</li>
						</ul>
						
					</div>
					
					<div class="tile-footer">
						<a href="#">View all tasks</a>
					</div>
					
				</div>
				
			</div>
			
			
			<div class="col-sm-3">
			
				<div class="tile-block tile-aqua">
					
					<div class="tile-header">
						<i class="glyphicon glyphicon-bullhorn"></i>
						
						<a href="#">
							Subscribe
							<span>Get the latest news!</span>
						</a>
					</div>
					
					<div class="tile-content">
					
						<p>Pleased him another was settled for. Moreover end horrible endeavor entrance any families.</p>
						
						<input type="text" class="form-control" placeholder="Enter your email..." />
						
						
					</div>
					
					<div class="tile-footer">
						<button type="button" class="btn btn-block">Subscribe</button>
					</div>
					
				</div>
				
			</div>
			
		</div>
		
		<br />
		
		
		<h2>Title - Tiles</h2>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-title tile-primary">
					
					<div class="icon">
						<i class="glyphicon glyphicon-leaf"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-red">
					
					<div class="icon">
						<i class="glyphicon glyphicon-lock"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-blue">
					
					<div class="icon">
						<i class="glyphicon glyphicon-link"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-aqua">
					
					<div class="icon">
						<i class="glyphicon glyphicon-random"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-title tile-cyan">
					
					<div class="icon">
						<i class="glyphicon glyphicon-leaf"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-purple">
					
					<div class="icon">
						<i class="glyphicon glyphicon-lock"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-green">
					
					<div class="icon">
						<i class="glyphicon glyphicon-link"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-pink">
					
					<div class="icon">
						<i class="glyphicon glyphicon-random"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-title tile-orange">
					
					<div class="icon">
						<i class="glyphicon glyphicon-leaf"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-brown">
					
					<div class="icon">
						<i class="glyphicon glyphicon-lock"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-plum">
					
					<div class="icon">
						<i class="glyphicon glyphicon-link"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-title tile-gray">
					
					<div class="icon">
						<i class="glyphicon glyphicon-random"></i>
					</div>
					
					<div class="title">
						<h3>Big Center Tile</h3>
						<p>so far in our blog, and our website.</p>
					</div>
				</div>
				
			</div>
		</div>
		
		
		<br />
		
		<h2>Stats - White Tiles</h2>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white tile-white-primary">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" &pound;" data-duration="1500" data-delay="0">0 &pound;</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-red">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-aqua">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-blue">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-cyan">
					<div class="icon"><i class="entypo-paper-plane"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-purple">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-pink">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-orange">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		<br />
		
		<div class="row">
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-green">
					<div class="icon"><i class="entypo-paper-plane"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-brown">
					<div class="icon"><i class="entypo-gauge"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-plum">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
			
			<div class="col-sm-3">
			
				<div class="tile-stats tile-white-gray">
					<div class="icon"><i class="entypo-suitcase"></i></div>
					<div class="num">83</div>
					
					<h3>Registered users</h3>
					<p>so far in our blog, and our website.</p>
				</div>
				
			</div>
		</div>
		
		
		<br />
		
		<h2>Group - Tiles</h2>
		
		<br />
		
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var map = $("#map");
				
				map.vectorMap({
					map: 'europe_merc_en',
					zoomMin: '3',
					backgroundColor: '#383f47',
					focusOn: { x: 0.5, y: 0.8, scale: 3 }
				});
			});
		</script>
		
		<div class="row">
			
			<div class="col-sm-12">
				
				<div class="tile-group">
					
					<div class="tile-left">
						<div class="tile-entry">
							<h3>Map</h3>
							<span>top visitors location</span>
						</div>
						
						<div class="tile-entry">
							<img src="assets/images/sample-al.png" alt="" class="pull-right op" />
							
							<h4>Albania</h4>
							<span>25%</span>
						</div>
						
						<div class="tile-entry">
							<img src="assets/images/sample-it.png" alt="" class="pull-right op" />
							
							<h4>Italy</h4>
							<span>18%</span>
						</div>
						
						<div class="tile-entry">
							<img src="assets/images/sample-au.png" alt="" class="pull-right op" />
							
							<h4>Austria</h4>
							<span>15%</span>
						</div>
					</div>
					
					<div class="tile-right">
						
						<div id="map" class="map"></div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2018 <strong> <a href="http://findhalal.de" target="_blank">Findhalal.der</a></strong>
		
		</footer>
	</div>

		
	<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
	
		<div class="chat-inner">
	
	
			<h2 class="chat-header">
				<a href="#" class="chat-close"><i class="entypo-cancel"></i></a>
	
				<i class="entypo-users"></i>
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
	
	
			<div class="chat-group" id="group-1">
				<strong>Favorites</strong>
	
				<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
			</div>
	
	
			<div class="chat-group" id="group-2">
				<strong>Work</strong>
	
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
			</div>
	
	
			<div class="chat-group" id="group-3">
				<strong>Social</strong>
	
				<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
			</div>
	
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
	
			<div class="conversation-header">
				<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
	
				<span class="user-status"></span>
				<span class="display-name"></span>
				<small></small>
			</div>
	
			<ul class="conversation-body">
			</ul>
	
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
	
		</div>
	
	</div>
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history">
		<li>
			<span class="user">Art Ramadani</span>
			<p>Are you here?</p>
			<span class="time">09:00</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>This message is pre-queued.</p>
			<span class="time">09:25</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>Whohoo!</p>
			<span class="time">09:26</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Catherine J. Watkins</span>
			<p>Do you like it?</p>
			<span class="time">09:27</span>
		</li>
	</ul>
	
	
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history_2">
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>I am going out.</p>
			<span class="time">08:21</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>Call me when you see this message.</p>
			<span class="time">08:27</span>
		</li>
	</ul>

	
</div>





	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">

	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>