<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Findhalal | Invoice</title>

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	@include('dashboard.sidebar')
	
	<div class="main-content">
							<ol class="breadcrumb bc-2 hidden-print" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="extra-icons.html">Extra</a>
							</li>
						<li class="active">
		
									<strong>Invoice</strong>
							</li>
							</ol>
					<br class="hidden-print" />
		
		<div class="invoice">
		
			<div class="row">
			
				<div class="col-sm-6 invoice-left">
				
					<a href="#">
						<img src="assets/images/laborator@2x.png" width="185" alt="" />
					</a>
					
				</div>
				
				<div class="col-sm-6 invoice-right">
				
						<h3>INVOICE NO. #5652256</h3>
						<span>31 October 2013</span>
				</div>
				
			</div>
			
			
			<hr class="margin" />
			
		
			<div class="row">
			
				<div class="col-sm-3 invoice-left">
				
					<h4>Client</h4>
					John Doe
					<br />
					Mr Nilson Otto
					<br />
					FoodMaster Ltd
					
				</div>
			
				<div class="col-sm-3 invoice-left">
					 
					<h4>&nbsp;</h4>
					1982 OOP
					<br />
					Madrid, Spain
					<br />
					+1 (151) 225-4183
				</div>
				
				<div class="col-md-6 invoice-right">
				
					<h4>Payment Details:</h4>
					<strong>V.A.T Reg #:</strong> 542554(DEMO)78
					<br />
					<strong>Account Name:</strong> FoodMaster Ltd
					<br />
					<strong>SWIFT code:</strong> 45454DEMO545DEMO
					
				</div>
				
			</div>
			
			<div class="margin"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th width="60%">Product</th>
						<th>Quantity</th>
						<th>Price</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="text-center">1</td>
						<td>On am we offices expense thought</td>
						<td>1</td>
						<td class="text-right">$1,290</td>
					</tr>
					
					<tr>
						<td class="text-center">2</td>
						<td>Up do view time they shot</td>
						<td>1</td>
						<td class="text-right">$400</td>
					</tr>
					
					<tr>
						<td class="text-center">3</td>
						<td>Way ham unwilling not breakfast</td>
						<td>1</td>
						<td class="text-right">$550</td>
					</tr>
					
					<tr>
						<td class="text-center">4</td>
						<td>Songs to an blush woman be sorry</td>
						<td>1</td>
						<td class="text-right">$4020</td>
					</tr>
					
					<tr>
						<td class="text-center">5</td>
						<td>Luckily offered article led lasting</td>
						<td>1</td>
						<td class="text-right">$87</td>
					</tr>
					
					<tr>
						<td class="text-center">6</td>
						<td>Of as by belonging therefore suspicion</td>
						<td>1</td>
						<td class="text-right">$140</td>
					</tr>
				</tbody>
			</table>
			
			<div class="margin"></div>
		
			<div class="row">
			
				<div class="col-sm-6">
				
					<div class="invoice-left">
			
						795 Park Ave, Suite 120
						<br />
						San Francisco, CA 94107
						<br />
						P: (234) 145-1810
						<br />
						Full Name
						<br />
						first.last@email.com
					</div>
				
				</div>
				
				<div class="col-sm-6">
					
					<div class="invoice-right">
						
						<ul class="list-unstyled">
							<li>
								Sub - Total amount: 
								<strong>$6,487</strong>
							</li>
							<li>
								VAT: 
								<strong>12.9%</strong>
							</li>
							<li>
								Discount: 
								-----
							</li>
							<li>
								Grand Total:
								<strong>$7,304</strong>
							</li>
						</ul>
						
						<br />
						
						<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
							Print Invoice
							<i class="entypo-doc-text"></i>
						</a>
						
						&nbsp;
						
						<a href="mailbox-compose.html" class="btn btn-success btn-icon icon-left hidden-print">
							Send Invoice
							<i class="entypo-mail"></i>
						</a>
					</div>
					
				</div>
				
			</div>
		
		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2018 <strong> <a href="http://findhalal.de" target="_blank">Findhalal.de</a></strong>
		
		</footer>
	</div>

		
	<div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
	
		<div class="chat-inner">
	
	
			<h2 class="chat-header">
				<a href="#" class="chat-close"><i class="entypo-cancel"></i></a>
	
				<i class="entypo-users"></i>
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
	
	
			<div class="chat-group" id="group-1">
				<strong>Favorites</strong>
	
				<a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
			</div>
	
	
			<div class="chat-group" id="group-2">
				<strong>Work</strong>
	
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
			</div>
	
	
			<div class="chat-group" id="group-3">
				<strong>Social</strong>
	
				<a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
			</div>
	
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
	
			<div class="conversation-header">
				<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
	
				<span class="user-status"></span>
				<span class="display-name"></span>
				<small></small>
			</div>
	
			<ul class="conversation-body">
			</ul>
	
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
	
		</div>
	
	</div>
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history">
		<li>
			<span class="user">Art Ramadani</span>
			<p>Are you here?</p>
			<span class="time">09:00</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>This message is pre-queued.</p>
			<span class="time">09:25</span>
		</li>
	
		<li class="opponent">
			<span class="user">Catherine J. Watkins</span>
			<p>Whohoo!</p>
			<span class="time">09:26</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Catherine J. Watkins</span>
			<p>Do you like it?</p>
			<span class="time">09:27</span>
		</li>
	</ul>
	
	
	
	
	<!-- Chat Histories -->
	<ul class="chat-history" id="sample_history_2">
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>I am going out.</p>
			<span class="time">08:21</span>
		</li>
	
		<li class="opponent unread">
			<span class="user">Daniel A. Pena</span>
			<p>Call me when you see this message.</p>
			<span class="time">08:27</span>
		</li>
	</ul>

	
</div>




	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>


	<!-- Imported scripts on this page -->
	<script src="assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>